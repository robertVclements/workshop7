package workshop7_webservices.DOA;

import workshop7_webservices.entity.CustomersEntity;

import javax.persistence.*;
import java.util.List;

public class CustomerDOA {


    private EntityManager entityManager;
    private EntityTransaction tx;

    public CustomerDOA() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("TravelExpertsRESTService");
        entityManager = factory.createEntityManager();
        tx = entityManager.getTransaction();
    }

    public List<CustomersEntity> getAllCustomers(){
            Query query = entityManager.createNamedQuery("Customer.findall", CustomersEntity.class);
            List<CustomersEntity> list = query.getResultList();
            return list;

    }

    public CustomersEntity getCustomer (int customerID){
            CustomersEntity customer = entityManager.find(CustomersEntity.class, customerID);
            return customer;
    }

    public void addCustomer (CustomersEntity customer){
        try {
            tx.begin();
            entityManager.persist(customer);
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }

    // TODO: 10/8/2018 add error checking to getting customer so if customer id doesnt exist
    public void deleteCustomer (int customerId){
        CustomersEntity customer = getCustomer(customerId);
        try {
            tx.begin();
            entityManager.remove(customer);
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }

    public void updateCustomer (CustomersEntity customersEntity){
        CustomersEntity customer = getCustomer(customersEntity.getCustomerId());
        try {
            customer.setCustFirstName(customersEntity.getCustFirstName());
            customer.setCustLastName(customersEntity.getCustLastName());
            customer.setCustAddress(customersEntity.getCustAddress());
            customer.setCustCity(customersEntity.getCustCity());
            customer.setCustProv(customersEntity.getCustProv());
            customer.setCustCountry(customersEntity.getCustCountry());
            customer.setCustHomePhone(customersEntity.getCustHomePhone());
            customer.setCustBusPhone(customersEntity.getCustBusPhone());
            customer.setCustEmail(customersEntity.getCustEmail());
            customer.setAgentId(customersEntity.getAgentId());
            customer.setUserid(customersEntity.getUserid());
            customer.setPasswd(customersEntity.getPasswd());
            tx.begin();
            entityManager.flush();
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }


}
