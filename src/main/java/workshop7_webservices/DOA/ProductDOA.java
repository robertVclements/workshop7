package workshop7_webservices.DOA;


import javassist.bytecode.stackmap.BasicBlock;
import workshop7_webservices.entity.ProductsEntity;

import javax.persistence.*;
import java.util.List;

public class ProductDOA {
    private EntityManager entityManager;
    private EntityTransaction tx;

    public ProductDOA() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("TravelExpertsRESTService");
        entityManager = factory.createEntityManager();
        tx = entityManager.getTransaction();
    }

    public List<ProductsEntity> getAllProducts(){
        Query query = entityManager.createNamedQuery("Products.findall", ProductsEntity.class);
        List<ProductsEntity> list = query.getResultList();
        return list;
    }

    public ProductsEntity getProduct(int productId){
        ProductsEntity productsEntity = entityManager.find(ProductsEntity.class, productId);
        return productsEntity;
    }

    public void addProduct (ProductsEntity productsEntity){
        try{
            tx.begin();
            entityManager.persist(productsEntity);
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }

    public void deleteProduct(int productId){
        ProductsEntity productsEntity = getProduct(productId);
        try{
            tx.begin();
            entityManager.remove(productsEntity);
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }

    public void updateProducts(ProductsEntity productsEntity){
        ProductsEntity products = getProduct(productsEntity.getProductId());
        try{
            products.setProdName(productsEntity.getProdName());
            tx.begin();
            entityManager.flush();
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }
}
