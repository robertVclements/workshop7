package workshop7_webservices.DOA;

import workshop7_webservices.entity.BookingsEntity;

import javax.persistence.*;
import java.util.List;

public class BookingDOA {

    private EntityManager entityManager;
    private EntityTransaction tx;

    public BookingDOA() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("TravelExpertsRESTService");
        entityManager = factory.createEntityManager();
        tx = entityManager.getTransaction();
    }

    public List<BookingsEntity> getAllBookings(){
        Query query = entityManager.createNamedQuery("Booking.findall", BookingsEntity.class);
        List<BookingsEntity> list = query.getResultList();
        return list;

    }

    public BookingsEntity getBooking(int bookingId) {
        BookingsEntity bookingsEntity = entityManager.find(BookingsEntity.class, bookingId);
        return bookingsEntity;
    }

    public void addBooking (BookingsEntity bookingsEntity){
        try{
            tx.begin();
            entityManager.persist(bookingsEntity);
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }

    public void deleteBookings (int bookingsId){
        BookingsEntity bookingsEntity = getBooking(bookingsId);
        try {
            tx.begin();
            entityManager.remove(bookingsEntity);
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }

    public void updateCustomer(BookingsEntity bookingsEntity){
        BookingsEntity booking = getBooking(bookingsEntity.getBookingId());
        try{
            booking.setBookingDate(bookingsEntity.getBookingDate());
            booking.setBookingNo(bookingsEntity.getBookingNo());
            booking.setCustomerId(bookingsEntity.getCustomerId());
            booking.setPackageId(bookingsEntity.getPackageId());
            booking.setTravelerCount(bookingsEntity.getTravelerCount());
            booking.setTripTypeId(bookingsEntity.getTripTypeId());
            tx.begin();
            entityManager.flush();
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }
}
