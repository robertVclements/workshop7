package workshop7_webservices.DOA;

import workshop7_webservices.entity.PackagesEntity;

import javax.persistence.*;
import java.util.List;

public class PackageDOA {

    private EntityManager entityManager;
    private EntityTransaction tx;

    public PackageDOA() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("TravelExpertsRESTService");
        entityManager = factory.createEntityManager();
        tx = entityManager.getTransaction();
    }

    public List<PackagesEntity> getAllPackages(){
        Query query = entityManager.createNamedQuery("Package.findall", PackagesEntity.class);
        List<PackagesEntity> lsit = query.getResultList();
        return lsit;
    }

    public PackagesEntity getPackage(int packageId){
        PackagesEntity packagesEntity = entityManager.find(PackagesEntity.class, packageId);
        return packagesEntity;
    }

    public void addPackage(PackagesEntity packagesEntity){
        try{
            tx.begin();
            entityManager.persist(packagesEntity);
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }

    public void deleteBookings (int packageId){
        PackagesEntity packagesEntity = getPackage(packageId);
        try {
            tx.begin();
            entityManager.remove(packagesEntity);
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }

    public void updatePackage(PackagesEntity packagesEntity){
        PackagesEntity packageNew = getPackage(packagesEntity.getPackageId());
        try{
            packageNew.setPkgName(packagesEntity.getPkgName());
            packageNew.setPkgStartDate(packagesEntity.getPkgStartDate());
            packageNew.setPkgEndDate(packagesEntity.getPkgEndDate());
            packageNew.setPkgDesc(packagesEntity.getPkgDesc());
            packageNew.setPkgBasePrice(packagesEntity.getPkgBasePrice());
            packageNew.setPkgAgencyCommission(packagesEntity.getPkgAgencyCommission());
            tx.begin();
            entityManager.flush();
            tx.commit();
        }
        catch (RuntimeException e){
            tx.rollback();
            throw e;
        }
    }
}
