package workshop7_webservices.services;

import workshop7_webservices.entity.CustomersEntity;
import workshop7_webservices.DOA.CustomerDOA;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("customers")
public class CustomerService {

    private CustomerDOA customerDOA = new CustomerDOA();
    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CustomersEntity> getCustomer(){
        List<CustomersEntity> list = customerDOA.getAllCustomers();
        return list;
    }

    @GET
    @Path("{customerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public CustomersEntity getCustomers(@PathParam("customerId") int customerId){
        return customerDOA.getCustomer(customerId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addCustomers(CustomersEntity customer){
        customerDOA.addCustomer(customer);
        return "Created";
    }

    @DELETE
    @Path("{customerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteCustomers (@PathParam("customerId") int customerId){
        customerDOA.deleteCustomer(customerId);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void updateCustomer (CustomersEntity customersEntity){
        customerDOA.updateCustomer(customersEntity);

    }

}
