package workshop7_webservices.services;

import org.eclipse.persistence.annotations.CascadeOnDelete;
import workshop7_webservices.DOA.ProductDOA;
import workshop7_webservices.entity.ProductsEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/products")
public class ProductService {

    private ProductDOA productDOA = new ProductDOA();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductsEntity> getAllProducts() {
        List<ProductsEntity> list = productDOA.getAllProducts();
        return list;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{productId}")
    public ProductsEntity getProduct(@PathParam("productId")int productId){
        ProductsEntity productsEntity = productDOA.getProduct(productId);
        return productsEntity;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addProducts(ProductsEntity productsEntity){
        productDOA.addProduct(productsEntity);
        return "Created";
    }

    @DELETE
    @Path("{productId}")
    public void deleteProduct(@PathParam("productId")int productId){
        productDOA.deleteProduct(productId);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void updateProduct(ProductsEntity productsEntity){
        productDOA.updateProducts(productsEntity);
    }

}
