package workshop7_webservices.services;

import workshop7_webservices.DOA.PackageDOA;
import workshop7_webservices.entity.PackagesEntity;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/packages")
public class PackagesService {

    private PackageDOA packageDOA = new PackageDOA();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<PackagesEntity> getAllPackage() {
        List<PackagesEntity> list = packageDOA.getAllPackages();
        return list;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{packageId}")
    public PackagesEntity getPackage(@PathParam("packageId")int packageId){
        PackagesEntity packagesEntity = packageDOA.getPackage(packageId);
        return packagesEntity;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String addPackage(PackagesEntity packagesEntity){
        packageDOA.addPackage(packagesEntity);
        return "Created";
    }

    @DELETE
    @Path("{packageId}")
    public void deletePackage(@PathParam("packageId")int packageId){
        packageDOA.deleteBookings(packageId);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updatePackage(PackagesEntity packagesEntity){
        packageDOA.updatePackage(packagesEntity);
    }
}
