package workshop7_webservices.services;

import workshop7_webservices.DOA.BookingDOA;
import workshop7_webservices.entity.BookingsEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

// The Java class will be hosted at the URI path "/helloworld"
@Path("/bookings")
public class BookingService {

    private BookingDOA bookingDOA = new BookingDOA();
    // The Java method will process HTTP GET requests
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<BookingsEntity> getAllBookings() {
        List<BookingsEntity> list = bookingDOA.getAllBookings();
        return list;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{bookingId}")
    public BookingsEntity getBooking(@PathParam("bookingId") int bookingId){
        BookingsEntity bookingsEntity = bookingDOA.getBooking(bookingId);
        return bookingsEntity;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addCustomers(BookingsEntity bookingsEntity){
        bookingDOA.addBooking(bookingsEntity);
        return "Created";
    }

    @DELETE
    @Path("{bookingId}")
    public void deleteBooking (@PathParam("bookingId") int bookingId){
        bookingDOA.deleteBookings(bookingId);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void updateBooking (BookingsEntity bookingsEntity){
        bookingDOA.updateCustomer(bookingsEntity);
    }
}
