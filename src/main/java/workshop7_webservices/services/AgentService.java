package workshop7_webservices.services;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;


@Path("/agents")
public class AgentService {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getLoginAgent() {
        return "blurg";
    }
}
