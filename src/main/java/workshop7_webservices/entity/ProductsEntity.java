package workshop7_webservices.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "products", schema = "travelexperts")
@NamedQuery(name = "Products.findall", query = "SELECT p from ProductsEntity p")
public class ProductsEntity {
    private int productId;
    private String prodName;

    @Id
    @Column(name = "ProductId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "ProdName")
    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductsEntity that = (ProductsEntity) o;
        return productId == that.productId &&
                Objects.equals(prodName, that.prodName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, prodName);
    }
}
