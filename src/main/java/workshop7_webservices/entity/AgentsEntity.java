package workshop7_webservices.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "agents", schema = "travelexperts", catalog = "")
public class AgentsEntity {
    private int agentId;
    private String agtFirstName;
    private String agtMiddleInitial;
    private String agtLastName;
    private String agtBusPhone;
    private String agtEmail;
    private String agtPosition;
    private Integer agencyId;
    private String agtUsername;
    private String agtPassword;

    @Id
    @Column(name = "AgentId")
    public int getAgentId() {
        return agentId;
    }

    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }

    @Basic
    @Column(name = "AgtFirstName")
    public String getAgtFirstName() {
        return agtFirstName;
    }

    public void setAgtFirstName(String agtFirstName) {
        this.agtFirstName = agtFirstName;
    }

    @Basic
    @Column(name = "AgtMiddleInitial")
    public String getAgtMiddleInitial() {
        return agtMiddleInitial;
    }

    public void setAgtMiddleInitial(String agtMiddleInitial) {
        this.agtMiddleInitial = agtMiddleInitial;
    }

    @Basic
    @Column(name = "AgtLastName")
    public String getAgtLastName() {
        return agtLastName;
    }

    public void setAgtLastName(String agtLastName) {
        this.agtLastName = agtLastName;
    }

    @Basic
    @Column(name = "AgtBusPhone")
    public String getAgtBusPhone() {
        return agtBusPhone;
    }

    public void setAgtBusPhone(String agtBusPhone) {
        this.agtBusPhone = agtBusPhone;
    }

    @Basic
    @Column(name = "AgtEmail")
    public String getAgtEmail() {
        return agtEmail;
    }

    public void setAgtEmail(String agtEmail) {
        this.agtEmail = agtEmail;
    }

    @Basic
    @Column(name = "AgtPosition")
    public String getAgtPosition() {
        return agtPosition;
    }

    public void setAgtPosition(String agtPosition) {
        this.agtPosition = agtPosition;
    }

    @Basic
    @Column(name = "AgencyId")
    public Integer getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Integer agencyId) {
        this.agencyId = agencyId;
    }

    @Basic
    @Column(name = "AgtUsername")
    public String getAgtUsername() {
        return agtUsername;
    }

    public void setAgtUsername(String agtUsername) {
        this.agtUsername = agtUsername;
    }

    @Basic
    @Column(name = "AgtPassword")
    public String getAgtPassword() {
        return agtPassword;
    }

    public void setAgtPassword(String agtPassword) {
        this.agtPassword = agtPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AgentsEntity that = (AgentsEntity) o;
        return agentId == that.agentId &&
                Objects.equals(agtFirstName, that.agtFirstName) &&
                Objects.equals(agtMiddleInitial, that.agtMiddleInitial) &&
                Objects.equals(agtLastName, that.agtLastName) &&
                Objects.equals(agtBusPhone, that.agtBusPhone) &&
                Objects.equals(agtEmail, that.agtEmail) &&
                Objects.equals(agtPosition, that.agtPosition) &&
                Objects.equals(agencyId, that.agencyId) &&
                Objects.equals(agtUsername, that.agtUsername) &&
                Objects.equals(agtPassword, that.agtPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(agentId, agtFirstName, agtMiddleInitial, agtLastName, agtBusPhone, agtEmail, agtPosition, agencyId, agtUsername, agtPassword);
    }
}
