package workshop7_webservices.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "customers", schema = "travelexperts")
@NamedQuery(name="Customer.findall", query="SELECT c FROM CustomersEntity c")
public class CustomersEntity {
    private int customerId;
    private String custFirstName;
    private String custLastName;
    private String custAddress;
    private String custCity;
    private String custProv;
    private String custPostal;
    private String custCountry;
    private String custHomePhone;
    private String custBusPhone;
    private String custEmail;
    private Integer agentId;
    private String userid;
    private String passwd;

    @Id
    @Column(name = "CustomerId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Basic
    @Column(name = "CustFirstName")
    public String getCustFirstName() {
        return custFirstName;
    }

    public void setCustFirstName(String custFirstName) {
        this.custFirstName = custFirstName;
    }

    @Basic
    @Column(name = "CustLastName")
    public String getCustLastName() {
        return custLastName;
    }

    public void setCustLastName(String custLastName) {
        this.custLastName = custLastName;
    }

    @Basic
    @Column(name = "CustAddress")
    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    @Basic
    @Column(name = "CustCity")
    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    @Basic
    @Column(name = "CustProv")
    public String getCustProv() {
        return custProv;
    }

    public void setCustProv(String custProv) {
        this.custProv = custProv;
    }

    @Basic
    @Column(name = "CustPostal")
    public String getCustPostal() {
        return custPostal;
    }

    public void setCustPostal(String custPostal) {
        this.custPostal = custPostal;
    }

    @Basic
    @Column(name = "CustCountry")
    public String getCustCountry() {
        return custCountry;
    }

    public void setCustCountry(String custCountry) {
        this.custCountry = custCountry;
    }

    @Basic
    @Column(name = "CustHomePhone")
    public String getCustHomePhone() {
        return custHomePhone;
    }

    public void setCustHomePhone(String custHomePhone) {
        this.custHomePhone = custHomePhone;
    }

    @Basic
    @Column(name = "CustBusPhone")
    public String getCustBusPhone() {
        return custBusPhone;
    }

    public void setCustBusPhone(String custBusPhone) {
        this.custBusPhone = custBusPhone;
    }

    @Basic
    @Column(name = "CustEmail")
    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    @Basic
    @Column(name = "AgentId")
    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    @Basic
    @Column(name = "userid")
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Basic
    @Column(name = "passwd")
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomersEntity that = (CustomersEntity) o;
        return customerId == that.customerId &&
                Objects.equals(custFirstName, that.custFirstName) &&
                Objects.equals(custLastName, that.custLastName) &&
                Objects.equals(custAddress, that.custAddress) &&
                Objects.equals(custCity, that.custCity) &&
                Objects.equals(custProv, that.custProv) &&
                Objects.equals(custPostal, that.custPostal) &&
                Objects.equals(custCountry, that.custCountry) &&
                Objects.equals(custHomePhone, that.custHomePhone) &&
                Objects.equals(custBusPhone, that.custBusPhone) &&
                Objects.equals(custEmail, that.custEmail) &&
                Objects.equals(agentId, that.agentId) &&
                Objects.equals(userid, that.userid) &&
                Objects.equals(passwd, that.passwd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, custFirstName, custLastName, custAddress, custCity, custProv, custPostal, custCountry, custHomePhone, custBusPhone, custEmail, agentId, userid, passwd);
    }
}
