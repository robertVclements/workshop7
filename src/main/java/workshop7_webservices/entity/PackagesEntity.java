package workshop7_webservices.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "packages", schema = "travelexperts")
@NamedQuery(name = "Package.findall", query = "SELECT p from PackagesEntity p")
public class PackagesEntity {
    private int packageId;
    private String pkgName;
    private Date pkgStartDate;
    private Date pkgEndDate;
    private String pkgDesc;
    private BigDecimal pkgBasePrice;
    private BigDecimal pkgAgencyCommission;

    @Id
    @Column(name = "PackageId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    @Basic
    @Column(name = "PkgName")
    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    @Basic
    @Column(name = "PkgStartDate")
    @Temporal(TemporalType.TIMESTAMP )
    public Date getPkgStartDate() {
        return pkgStartDate;
    }

    public void setPkgStartDate(Date pkgStartDate) {
        this.pkgStartDate = pkgStartDate;
    }

    @Basic
    @Column(name = "PkgEndDate")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getPkgEndDate() {
        return pkgEndDate;
    }

    public void setPkgEndDate(Date pkgEndDate) {
        this.pkgEndDate = pkgEndDate;
    }

    @Basic
    @Column(name = "PkgDesc")
    public String getPkgDesc() {
        return pkgDesc;
    }

    public void setPkgDesc(String pkgDesc) {
        this.pkgDesc = pkgDesc;
    }

    @Basic
    @Column(name = "PkgBasePrice")
    public BigDecimal getPkgBasePrice() {
        return pkgBasePrice;
    }

    public void setPkgBasePrice(BigDecimal pkgBasePrice) {
        this.pkgBasePrice = pkgBasePrice;
    }

    @Basic
    @Column(name = "PkgAgencyCommission")
    public BigDecimal getPkgAgencyCommission() {
        return pkgAgencyCommission;
    }

    public void setPkgAgencyCommission(BigDecimal pkgAgencyCommission) {
        this.pkgAgencyCommission = pkgAgencyCommission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackagesEntity that = (PackagesEntity) o;
        return packageId == that.packageId &&
                Objects.equals(pkgName, that.pkgName) &&
                Objects.equals(pkgStartDate, that.pkgStartDate) &&
                Objects.equals(pkgEndDate, that.pkgEndDate) &&
                Objects.equals(pkgDesc, that.pkgDesc) &&
                Objects.equals(pkgBasePrice, that.pkgBasePrice) &&
                Objects.equals(pkgAgencyCommission, that.pkgAgencyCommission);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageId, pkgName, pkgStartDate, pkgEndDate, pkgDesc, pkgBasePrice, pkgAgencyCommission);
    }
}
